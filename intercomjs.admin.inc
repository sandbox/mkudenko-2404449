<?php
/**
 * @file
 * Provides admin interface.
 */


/**
 * Intercom admin settings form.
 */
function intercomjs_settings_form($form, &$form_state) {
  $form['intercomjs_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Intercom APP ID'),
    '#size' => 10,
    '#required' => TRUE,
    '#default_value' => variable_get(INTERCOMJS_APP_ID),
  );

  $form['intercomjs_track_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Track user 1'),
    '#default_value' => variable_get(INTERCOMJS_TRACK_ADMIN),
  );

  $form['actions'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    ),
  );

  return $form;
}


/**
 * Submit handler for Intercom admin settings form.
 */
function intercomjs_settings_form_submit($form, &$form_state) {
  variable_set(INTERCOMJS_APP_ID, $form_state['values']['intercomjs_app_id']);
  variable_set(INTERCOMJS_TRACK_ADMIN, $form_state['values']['intercomjs_track_admin']);

  drupal_set_message(t('Intercom configuration has been saved.'));
}


/**
 * Intercom event form.
 */
function intercomjs_event_form($form, &$form_state, $event_id = '') {
  $form['event_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Event name'),
    '#required' => TRUE,
  );

  $form['event_type'] = array(
    '#type' => 'select',
    '#title' => t('Event type'),
    '#options' => intercomjs_event_types(),
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => 'Use &lt;front&gt; for homepage.',
    '#states' => array(
      'visible' => array(
        ':input[name="event_type"]' => array('value' => 'page_viewed'),
      ),
      'required' => array(
        ':input[name="event_type"]' => array('value' => 'page_viewed'),
      ),
    ),
  );

  $form['actions'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save event'),
      '#weight' => 1,
    ),
  );

  $event = intercomjs_event_load($event_id);
  if (!empty($event)) {
    $form_state['event'] = $event;
    drupal_set_title(t('Edit @name event', array('@name' => $event['name'])));
    $form['event_name']['#default_value'] = $event['name'];
    $form['event_type']['#default_value'] = $event['type'];
    $form['url']['#default_value'] = $event['url'];
    $form['actions']['submit']['#value'] = 'Update event';
    $form['actions']['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Back to events'),
      '#submit' => array('intercomjs_event_form_cancel'),
      '#weight' => 0,
    );
  }

  return $form;
}


/**
 * Submit handler for Intercom event form.
 */
function intercomjs_event_form_submit($form, &$form_state) {
  $action = 'updated';

  if (!isset($form_state['event'])) {
    $event = array();
    $action = 'saved';
  }

  $event['name'] = $form_state['values']['event_name'];
  $event['type'] = $form_state['values']['event_type'];

  $url = trim(trim($form_state['values']['url'], "/"));
  $event['url'] = $url;

  intercomjs_event_add_hash($event);

  $events[$event['id']] = $event;

  variable_set(INTERCOMJS_EVENTS, serialize($events));

  drupal_set_message(t('%name has been @action.', array(
    '%name' => $event['name'],
    '@action' => $action,
  )));

  drupal_goto('admin/config/services/intercom/events');
}


/**
 * Intercom delete event form.
 */
function intercomjs_event_delete_form($form, &$form_state, $event_id) {
  $events = intercomjs_get_events_array();
  $event = $events[$event_id];
  $form_state['events'] = $events;
  $form_state['event_id'] = $event_id;
  drupal_set_title(t('Delete @name event', array('@name' => $event['name'])));

  $form['warning'] = array(
    '#markup' => '<p>Are you sure you want to delete the ' . $event['name'] . ' event? The tracking history will still be available in Intercom.</p>
      <p>You can recreate an event in the future should you want to resume tracking.</p>',
  );

  $form['actions'] = array(
    'cancel' => array(
      '#type' => 'submit',
      '#value' => t('Back to events'),
      '#submit' => array('intercomjs_event_form_cancel'),
      '#weight' => 0,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Delete event'),
      '#weight' => 1,
    ),
  );

  return $form;
}


/**
 * Cancel callback for event form.
 */
function intercomjs_event_form_cancel($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/services/intercom/events';
}


/**
 * Submit callback for event delete confirmation form.
 */
function intercomjs_event_delete_form_submit($form, &$form_state) {
  $events = $form_state['events'];
  $event_id = $form_state['event_id'];
  $event = $events[$event_id];

  unset($events[$event_id]);
  variable_set(INTERCOMJS_EVENTS, serialize($events));

  drupal_set_message(t('%name event has been deleted.', array('%name' => $event['name'])));

  $form_state['redirect'] = 'admin/config/services/intercom/events';
}


/**
 * Intercom events page callback.
 */
function intercomjs_events_list_page() {
  $header = array(
    'Name',
    'Type',
    'Details',
    'Edit',
    'Delete',
  );
  $rows = array();
  $events = intercomjs_get_events_array();
  $event_types = intercomjs_event_types();

  foreach ($events as $event) {
    $rows[] = array(
      'data' => array(
        $event['name'],
        $event_types[$event['type']],
        $event['url'],
        l(t('Edit'), 'admin/config/services/intercom/events/' . $event['id'] . '/edit'),
        l(t('Delete'), 'admin/config/services/intercom/events/' . $event['id'] . '/delete'),
      ),
    );
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));
}


/**
 * Defines a list of available event types.
 */
function intercomjs_event_types() {
  return array(
    'page_viewed' => t('Page viewed'),
  );
}


/**
 * Generates a unique ID for the event.
 *
 * @param $event
 *
 * @return mixed
 */
function intercomjs_event_add_hash(&$event) {
  if (!isset($event['id'])) {
    $serialized = serialize($event) . REQUEST_TIME;
    $event['id'] = hash('md5', $serialized);
  }
}
