INTRODUCTION
------------
The IntercomJS module provides JavaScript integration with Intercom.IO. It
injects a bit of JavaScript into the page template and submits events to
Intercom.


FEATURES
--------
1. Tracking of basic user info - user name, user id, email, created date.
2. Page view events.
3. Tracking of user 1 can be disabled (all users are tracked by default).


INSTALLATION
------------
1. Download and enable the module as usual.
2. Visit admin/config/services/intercom/settings to specify the APP ID.
   Alternatively, you can edit your settings.php file, which is handy for
   handling different environments.
   <?php
     if (in_array(PANTHEON_ENVIRONMENT, array('live'))) {
       $conf['intercomjs_app_id'] = 'abcdefgh';
     }
     else {
       $conf['intercomjs_app_id'] = 'ijklmnop';
     }
   ?>
3. Insert the following snippet into your html.tpl.php just before
   the closing <body> tag.
   <?php if (isset($intercomjs)): ?>
     <?php print $intercomjs; ?>
   <?php endif; ?>


MAINTAINERS
-----------
Current maintainers:
 * Michael Kudenko (mkudenko) - https://www.drupal.org/u/mkudenko

This project has been sponsored by:
 * Webscope - leading Drupal shop in Auckland, New Zealand.
   Visit http://webscope.co.nz for more details.