// Using the closure to map jQuery to $.
(function ($) {

  $(document).ready(function () {
    var eventName = Drupal.settings.intercomEvent;
    if (eventName !== undefined) {
      Intercom('trackEvent', eventName);
    }
  });

}(jQuery));
