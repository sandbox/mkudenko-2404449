<?php
/**
 * @file
 *   Integrates Drupal with Intercom IO via JavaScript API.
 */


define('INTERCOMJS_APP_ID', 'intercomjs_app_id');
define('INTERCOMJS_TRACK_ADMIN', 'intercomjs_track_admin');
define('INTERCOMJS_EVENTS', 'intercomjs_events');


/**
 * Implements hook_permission().
 */
function intercomjs_permission() {
  return array(
    'administer intercom settings' =>  array(
      'title' => t('Administer Intercom settings'),
      'restrict access' => TRUE,
    ),
  );
}


/**
 * Implements hook_menu().
 */
function intercomjs_menu() {
  $items = array();

  $items['admin/config/services/intercom'] = array(
    'title'            => 'Intercom',
    'access arguments' => array('administer intercom settings'),
  );

  $items['admin/config/services/intercom/settings'] = array(
    'title'            => 'Intercom API Settings',
    'description'      => 'Manage Intercom API settings',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('intercomjs_settings_form'),
    'access arguments' => array('administer intercom settings'),
    'file'             => 'intercomjs.admin.inc',
    'weight'           => 0,
  );

  $items['admin/config/services/intercom/events'] = array(
    'title'            => 'Events',
    'description'      => 'Manage events submitted to Intercom',
    'page callback'    => 'intercomjs_events_list_page',
    'access arguments' => array('administer intercom settings'),
    'file'             => 'intercomjs.admin.inc',
    'weight'           => 1,
  );

  $items['admin/config/services/intercom/events/all'] = array(
    'title'            => 'Events',
    'access arguments' => array('administer intercom settings'),
    'type'             => MENU_DEFAULT_LOCAL_TASK,
    'weight'           => 0,
  );

  $items['admin/config/services/intercom/events/add'] = array(
    'title'            => 'Add event',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('intercomjs_event_form'),
    'access arguments' => array('administer intercom settings'),
    'type'             => MENU_LOCAL_ACTION,
    'file'             => 'intercomjs.admin.inc',
    'weight'           => 0,
  );

  $items['admin/config/services/intercom/events/%/edit'] = array(
    'title'            => 'Edit event',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('intercomjs_event_form', 5),
    'access arguments' => array('administer intercom settings'),
    'file'             => 'intercomjs.admin.inc',
    'weight'           => 0,
  );

  $items['admin/config/services/intercom/events/%/delete'] = array(
    'title'            => 'Delete event',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('intercomjs_event_delete_form', 5),
    'access arguments' => array('administer intercom settings'),
    'file'             => 'intercomjs.admin.inc',
  );

  return $items;
}


/**
 * Implements hook_theme().
 */
function intercomjs_theme($existing, $type, $theme, $path) {
  return array(
    'intercomjs_snippet' => array(
      'template' => 'intercomjs-snippet',
    ),
  );
}


/**
 * Implements template_preprocess_html().
 */
function intercomjs_preprocess_html(&$vars) {
  if (user_is_anonymous()) {
    return;
  }

  $app_id = variable_get(INTERCOMJS_APP_ID);
  if (!$app_id) {
    return;
  }

  global $user;
  $track_admin = variable_get(INTERCOMJS_TRACK_ADMIN);

  if (!$track_admin && $user->uid == 1) {
    return;
  }

  $intercom_vars = array(
    'fullname'   => format_username($user),
    'email'      => $user->mail,
    'created_at' => $user->created,
    'uid'        => $user->uid,
    'app_id'     => $app_id,
  );
  $vars['intercomjs'] = theme('intercomjs_snippet', $intercom_vars);

  // Handle events.
  $events = intercomjs_get_events_array();
  $submit_event = FALSE;
  foreach ($events as $event) {
    if ($event['type'] == 'page_viewed') {
      if ($event['url'] == '<front>' && drupal_is_front_page()) {
        $submit_event = TRUE;
      }
      elseif ($event['url'] == current_path()) {
        $submit_event = TRUE;
      }

      if ($submit_event) {
        drupal_add_js(drupal_get_path('module', 'intercomjs') . '/intercomjs.js', 'file');
        intercomjs_submit_event($event['name']);
        break;
      }
    }
  }
}


/**
 * Loads an events array from the database.
 */
function intercomjs_get_events_array() {
  $serialized = variable_get(INTERCOMJS_EVENTS);

  return unserialize($serialized);
}


/**
 * Sends event array to JS.
 *
 * @param $event_name
 */
function intercomjs_submit_event($event_name) {
  drupal_add_js(array('intercomEvent' => $event_name), 'setting');
}


/**
 * Loads event data from the database.
 *
 * @param $event_id
 */
function intercomjs_event_load($event_id) {
  $events = intercomjs_get_events_array();

  return ($event_id) ? $events[$event_id] : NULL;
}
